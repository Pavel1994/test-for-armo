﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using TestforARMO.Properties;

namespace TestforARMO
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();

            StartPath = Properties.Settings.Default.StartPath;
            FileName = Properties.Settings.Default.FileName;
            ContentText = Properties.Settings.Default.ContentText;
            StartPathTextBox.Text = StartPath;
            FileNameTextBox.Text = FileName;
            ContentTextTextBox.Text = ContentText;
            
        }
                
        string StartPath;                                   //Стартовая директория для поиска
        string FileName;                                    //Шаблон имени файла
        string ContentText;                                 //Содержание файла
        List<string> foundfileList = new List<string>();    //Список найденных файлов
        int numFile = 0;                                    //Кол-во обработанных файлов
        int numFoundFile = 0;                               //Кол-во найденных файлов
        string NowFile;                                     //Обработываемый файл

        
        int startbutton = 0;                                //Состояние кнопки "Старт"
        
        int hours, sek, min;                                //Часы, минуты, секунды
        
        public Thread SearchThread;                         //Поток выполнения поиска


        private void Form1_Load_1(object sender, EventArgs e)
        {
            
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //запись настроек формы
            Properties.Settings.Default.StartPath = StartPathTextBox.Text;
            Properties.Settings.Default.FileName = FileNameTextBox.Text;
            Properties.Settings.Default.ContentText = ContentTextTextBox.Text;
            //сохранение настроек
            Properties.Settings.Default.Save();
            //SearchThread.Abort();    
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }


        //Выбор стартовой директории поиска
        private void FolderBrowserButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog FBD = new FolderBrowserDialog();
            FBD.ShowNewFolderButton = true;
            if (FBD.ShowDialog() == DialogResult.OK)
            {
                StartPathTextBox.Text = FBD.SelectedPath;           
            }
        }
        
        //Кнопка начала поиска
        private void ButtonStartSearch_Click(object sender, EventArgs e)
        {
            if (startbutton == 0)
            {
                SearchThread = new Thread(SearchFile);
                SearchThread.Start();
                timer1.Enabled = true;
                buttonStartSearch.Enabled = false;

            }
            else
            {
                SearchThread.Resume();
                statuslabel.Text = "Поиск идет";
                statuslabel.Update();
                timer1.Enabled = true;
                buttonStartSearch.Enabled = false;
                PauseButton.Enabled = true;
            }           
            
        }

        //Кнопка паузы
        private void PauseButton_Click(object sender, EventArgs e)
        {
            SearchThread.Suspend();

            PauseButton.Enabled = false;
            ClearButton.Enabled = true;
            ClearResultButton.Enabled = true;
            buttonStartSearch.Text = "Продолжить поиск";
            buttonStartSearch.Enabled = true;
            startbutton = 1;
            timer1.Enabled = false;

            statuslabel.Text = "Поиск остановлен";
            statuslabel.Update();
        }


        private void StartPathTextBox_TextChanged(object sender, EventArgs e)
        {
            StartPath = StartPathTextBox.Text;
            
        }
        
        private void FileNameTextBox_TextChanged(object sender, EventArgs e)
        {
            FileName = FileNameTextBox.Text;
        }

        private void ContentTextTextBox_TextChanged(object sender, EventArgs e)
        {
            ContentText = ContentTextTextBox.Text;
        }

        //Кнопка очистки формы и результатов
        private void ClearButton_Click(object sender, EventArgs e)
        {
            StartPath = null;
            StartPathTextBox.Text = StartPath;

            ContentText = null;
            ContentTextTextBox.Text = ContentText;

            FileName = null;
            FileNameTextBox.Text = FileName;

            foundfileList.Clear();

            NowFile = null;

            numFile = 0;
            numFoundFile = 0;

            labelnowfile.Text = null;
            labelnumFile.Text = null;
            labelnumFoundFile.Text = null;

            statuslabel.Text = null;
            statuslabel.Update();

            buttonStartSearch.Text = "Начать поиск";
            startbutton = 0;

            hours = 0; sek = 0; min = 0;


            listView1.Items.Clear();
        }

        //Кнопка очистки результатов
        private void ClearResultButton_Click(object sender, EventArgs e)
        {
            foundfileList.Clear();

            numFile = 0;
            numFoundFile = 0;
            NowFile = null;
                        
            labelnowfile.Text = null;
            labelnumFile.Text = null;
            labelnumFoundFile.Text = null;

            statuslabel.Text = null;
            statuslabel.Update();

            buttonStartSearch.Text = "Начать поиск";
            startbutton = 0;

            hours = 0; sek = 0; min = 0;

            listView1.Items.Clear();
        }

        private void ListView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.FocusedItem != null)
            {
                Process.Start(foundfileList[listView1.FocusedItem.Index]);
            }
        }
                

        private void Timer1_Tick(object sender, EventArgs e)
        {
            sek += 1;
            if (sek == 60)
            {
                sek = 0;
                min += 1;
            }
            if (min == 60)
            {
                min = 0;
                hours += 1;
            }
            Timelabel.Text = string.Format("{0}:{1}:{2}", hours.ToString().PadLeft(2, '0'), min.ToString().PadLeft(2, '0'), sek.ToString().PadLeft(2, '0'));
        }



        //Метод поиска файлов
        public void SearchFile()
        {
            this.Invoke((MethodInvoker)delegate ()              
            {
                PauseButton.Enabled = true;
                ClearButton.Enabled = false;
                ClearResultButton.Enabled = false;
                statuslabel.Text = "Поиск идет";
                statuslabel.Update();               
            });
            foreach (string FindedFile in Directory.EnumerateFiles(StartPath, FileName, SearchOption.AllDirectories))
            {
                FileInfo FI;
                FI = new FileInfo(FindedFile);
                NowFile = FI.Name;
                numFile = numFile + 1;
                this.Invoke((MethodInvoker)delegate ()
                {
                    labelnowfile.Text = NowFile;
                    labelnowfile.Update();                              //Показывает имя проверяемого файла                    
                    labelnumFile.Text = Convert.ToString(numFile);
                    labelnumFile.Update();                              //Показывает количество обработанных файлов заданного типа                    
                });
                Thread.Sleep(2000);                                     //Задержка для наглядности процесса
                
                try
                {
                    string tmp = File.ReadAllText(FindedFile);
                    if (tmp.IndexOf(ContentText, StringComparison.CurrentCulture) != -1)
                    {
                        numFoundFile = numFoundFile + 1;                //Считает количество найденных
                        foundfileList.Add(FindedFile);                       

                        this.Invoke((MethodInvoker)delegate ()
                        {
                            labelnumFoundFile.Text = Convert.ToString(numFoundFile);
                            labelnumFoundFile.Update();                     //Показывает количество найденных файлов
                            imageList1.Images.Add(System.Drawing.Icon.ExtractAssociatedIcon(FI.FullName));
                            listView1.Items.Add(FI.Name, imageList1.Images.Count - 1);      //Добавляет в listView
                            listView1.Update();
                        });
                    }

                }
                catch { continue; }     //Ошибка
            }
            this.Invoke((MethodInvoker)delegate ()
            {
                PauseButton.Enabled = false;
                ClearButton.Enabled = true;
                ClearResultButton.Enabled = true;
                buttonStartSearch.Text = "Начать поиск";
                buttonStartSearch.Enabled = true;
                startbutton = 0;

                statuslabel.Text = "Поиск завершен";
                statuslabel.Update();
            });
            timer1.Enabled = false;                         
        }
        
    }
    
}
